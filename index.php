<?php
require 'includes/flight/flight/Flight.php';
require 'includes/smarty/libs/Smarty.class.php';

require('routes.php');

session_start();
if(empty($_SESSION['lang'])) { 
    $_SESSION['lang'] = "FR";
}
if ($_SESSION['lang'] == "FR")
    include 'langs/fr.php';
else
    include 'langs/en.php';

Flight::register('view', 'Smarty', array(), function($smarty)
{
    $smarty->template_dir='templates/';
    $smarty->compile_dir='templates_c/';
    $smarty->config_dir='config/';
    $smarty->cache_dir='cache/';
});

Flight::map('render', function($template, $data)
{     
    Flight::view()->assign($data);
    Flight::view()->display($template);
});

Flight::start();
?>

