<?php
Flight::route('/*', function($route){
    $lang = substr($route->splat, -2);
    if ($lang == "FR" || $lang == "EN") {
        $_SESSION['lang'] = $lang;
        if (strpos($route->splat, "/"))
            $lang = '/' . $lang;
        $redirect = str_replace("$lang", "", $route->splat);
        Flight::redirect("/$redirect");
    }
    else
        Flight::notFound();
}, true);