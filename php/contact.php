<?php
Flight::route('GET /contact', function(){
    Flight::render('contact.tpl', Array('NAVBAR'=>NAVBAR, 'CONTACT'=>CONTACT, 'lang'=>$_SESSION['lang']));
});

Flight::route('POST /contact', function(){
    $send = false;
    if (!empty($_POST['contact_name']) && strlen($_POST['contact_name']) <= 75
        && !empty($_POST['contact_email']) && filter_var($_POST['contact_email'], FILTER_VALIDATE_EMAIL)
        && strlen($_POST['contact_subject']) <= 75
        && !empty($_POST['contact_message']) && strlen($_POST['contact_message']) <= 5000) {
        include '../recaptchaKey.php';
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=" . $secretKey . "&response=" . $_POST['recaptcha_response'];
        $response = file_get_contents($url);
        if ((!is_null($response)) && !empty($response)) {
            $jsonResponse = json_decode($response);
            if ($jsonResponse->success && $jsonResponse->action === "contact" && $jsonResponse->score >= 0.5) {
                $subject = "[Contact form - www.benoit-chevillon.fr]";
                if (!empty($_POST['contact_subject']))
                    $subject = $subject . ' ' . $_POST['contact_subject'];
                $message = $_POST['contact_message'] . "\r\n\r\nAutomatically sent from the contact form.\r\nReCAPTCHA score : " . $jsonResponse->score;
                $headers = array(
                    'From' => $_POST['contact_name'] . ' ' . '<' . $_POST['contact_email'] . '>',
                    'X-Mailer' => 'PHP/' . phpversion());
                mail('benoit.chevillon6@gmail.com', $subject, $message, $headers);
                $send = true;
            }
        }
    }
    Flight::render('contact.tpl', Array('NAVBAR'=>NAVBAR, 'CONTACT'=>CONTACT, 'lang'=>$_SESSION['lang'], 'send'=>$send));
});