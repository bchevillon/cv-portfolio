<?php
class Work
{
    public $titleFR;
    public $titleEN;
    public $presentationFR;
    public $presentationEN;
    public $image;
    public $urlWork;
    public $urlPresentation;
    public $labels;

    public function __construct($titleFR, $titleEN, $presentationFR, $presentationEN, $image, $urlWork, $urlPresentation, $labelTitles)
    {
        $this->titleFR = $titleFR;
        $this->titleEN = $titleEN;
        $this->presentationFR = $presentationFR;
        $this->presentationEN = $presentationEN;
        $this->titleFR = $titleFR;
        $this->image = $image;
        $this->urlWork = $urlWork;
        $this->urlPresentation = $urlPresentation;
        $labels = [];
        foreach ($labelTitles as $labelTitle) {
            switch ($labelTitle) {
                case "HTML/CSS":
                    $labels["HTML/CSS"] = "primary";
                    break;
                case "JS":
                    $labels["JS"] = "warning";
                    break;
                case "PHP":
                    $labels["PHP"] = "info";
                    break;
                case "SQL":
                    $labels["SQL"] = "light";
                    break;
                case "JAVA":
                    $labels["JAVA"] = "danger";
                    break;
                case "XML":
                    $labels["XML"] = "dark";
                    break;
                case "C":
                    $labels["C"] = "secondary";
                    break;
                case "C++":
                    $labels["C++"] = "secondary";
                    break;
                case "C#":
                    $labels["C#"] = "success";
                    break;
            }
        }
        $this->labels = $labels;
    }
}

Flight::route('/works', function() {
    $quizIut = new Work(
        "Quiz : découvrez le remplaçant du DUT",
        "Quiz : discover the DUT replacement",
        "Quiz interactif centré sur le fonctionnement des IUT et des BUT (formation d'étude). 
        Vérification serveur de la réponse proposée, calcul du taux de réussite par rapport à l'ensemble des joueurs. 
        Classement général calculé en fin de quiz.",
        "Interactive quiz focused on the functioning of IUTs and BUTs (French study).
         Server verification of the proposed response, calculation of the success rate compared to all players.
         General rank calculated at the end of the quiz. ",
        "quiz-iut/quiz-iut.png",
        "http://www.benoit-chevillon.fr/works/quiz-iut/app",
        "quiz-iut/presentation",
        ["HTML/CSS", "JS", "PHP", "SQL"]);
    $festivalApplicationManager = new Work(
        "Gestionnaire de candidature de festival",
        "Festival application manager",
        "Site web permettant de candidater pour un festival fictif.
        Les comptes administrateurs ont la possibilité de gérer les candidatures et les comptes utilisateurs grâce aux différents outils.
        Une API légère est accessible afin de récupérer différentes données.",
        "Website to apply for a fictitious music festival.
        Administrator accounts can manage applications and user accounts through the various tools.
        A lightweight API is publicly accessible to easily retrieve different data on the website.",
        "festival-application-manager/festival-application-manager.png",
        "",
        "festival-application-manager/presentation",
        ["HTML/CSS", "JS", "PHP", "SQL"]);
    $colorMemory = new Work(
        "Jeu : \"Color Memory\"",
        "Game : \"Color Memory\"",
        "Application Android respectant les règles du jeu \"Color Memory\".
        Différents modes de jeux et niveaux sont disponibles.
        Les joueurs peuvent augmenter leur score différemment en fonction des modes et des difficultés. 
        Un classement permet également aux joueurs de se confronter.",
        "Android application respecting the rules of the game \"Color Memory\".
        Different game modes and levels are available.
        Players can increase their score differently depending on modes and difficulties.
        A ranking also allows players to confront each other.",
        "color-memory/color-memory.jpg",
        "",
        "color-memory/presentation",
        ["JAVA", "XML"]);
    $PHPScenariOpale = new Work(
        "Module de cours PHP - Scenari Opale",
        "PHP course module - Scenari Opale",
        "Module de cours basé sur la chaine éditoriale Opale grâce au logiciel Scenari.
        Ce projet est une application concrète de ces outils.
        Introduction aux templates et au routing PHP à travers le framework Flight et le moteur de templates Smarty.",
        "Course module using the Opale editorial chain thanks to the Scenari software. 
        This project is a concrete application of these tools.
        Introduction to templates and PHP routing through the Flight framework and the Smarty template engine.",
        "php-scenari-opale/php-scenari-opale.png",
        "",
        "php-scenari-opale/presentation",
        ["PHP", "XML"]);
    $osboseGame = new Work(
        "Jeu vidéo conçu avec Godot : Osbose",
        "Video game designed with Godot: Osbose",
        "Jeu vidéo de plateforme coopératif réalisé grâce au moteur libre Godot.
        Ce projet est le résultat d'une étude réalisé dans le cadre d'une formation à l'UTC.
        Toujours séparé mais à portée de vue dans les différents niveaux, les deux personnages doivent coopérer dans un temps limité.",
        "Cooperative platform video game playable on the same computer, made thanks to the Godot engine.
        This project is the result of a study carried out as part of a training at the UTC.
        Always separated but within sight in the different levels, the two characters must cooperate in a limited time.",
        "osbose-game/osbose-game.jpg",
        "",
        "osbose-game/presentation",
        []);
    $worksList = [$quizIut, $festivalApplicationManager, $colorMemory, $PHPScenariOpale, $osboseGame];
    Flight::render('works.tpl', Array('NAVBAR'=>NAVBAR, 'WORKS'=>WORKS, 'worksList'=>$worksList, 'lang'=>$_SESSION['lang']));
});