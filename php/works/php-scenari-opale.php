<?php
Flight::route('/works/php-scenari-opale/app', function(){
    Flight::redirect('/external-apps/php-scenari-opale');
});

Flight::route('/works/php-scenari-opale', function(){
    Flight::redirect("/works/php-scenari-opale/app");
});

Flight::route('/works/php-scenari-opale/presentation', function(){
    Flight::render('works/php-scenari-opale/php-scenari-opale_presentation.tpl',
        Array('NAVBAR'=>NAVBAR, 'PHP_SCENARI_OPALE'=>PHP_SCENARI_OPALE, 'lang'=>$_SESSION['lang']));
});