<?php
Flight::route('/works/color-memory/presentation', function(){
    Flight::render('works/color-memory/color-memory_presentation.tpl',
        Array('NAVBAR'=>NAVBAR, 'COLOR_MEMORY'=>COLOR_MEMORY, 'lang'=>$_SESSION['lang']));
});

Flight::route('/works/color-memory', function(){
    Flight::redirect("/works/color-memory/presentation");
});