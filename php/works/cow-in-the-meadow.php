<?php
Flight::route('/works/cow-in-the-meadow/app', function(){
    Flight::view()->display('works/cow-in-the-meadow/cow-in-the-meadow.html');
});

Flight::route('/works/cow-in-the-meadow', function(){
    Flight::redirect("/works/cow-in-the-meadow/app");
});