<?php
Flight::route('/works/osbose-game/presentation', function(){
    Flight::render('works/osbose-game/osbose-game_presentation.tpl',
        Array('NAVBAR'=>NAVBAR, 'OSBOSE_GAME'=>OSBOSE_GAME, 'lang'=>$_SESSION['lang']));
});

Flight::route('/works/osbose-game', function(){
    Flight::redirect("/works/osbose-game/presentation");
});