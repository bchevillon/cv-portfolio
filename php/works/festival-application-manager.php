<?php
Flight::route('/works/festival-application-manager/presentation', function(){
    Flight::render('works/festival-application-manager/festival-application-manager_presentation.tpl',
        Array('NAVBAR'=>NAVBAR, 'FESTIVAL_APPLICATION_MANAGER'=>FESTIVAL_APPLICATION_MANAGER, 'lang'=>$_SESSION['lang']));
});

Flight::route('/works/festival-application-manager', function(){
    Flight::redirect("/works/festival-application-manager/presentation");
});