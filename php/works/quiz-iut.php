<?php
Flight::route('/works/quiz-iut/app', function(){
    Flight::view()->display('works/quiz-iut/quiz-iut.html');
});

Flight::route('/works/quiz-iut', function(){
    Flight::redirect("/works/quiz-iut/app");
});

Flight::route('/works/quiz-iut-api/@info(/@reponse)', function($info, $response){
    include '../pdo.php';
    if (isset($response)) { // In this case, @info contains the number of the question and @response the number of the answer
        define('responses', array(1,4,3,1,2,1,2,1,3,3,4,2,2,1,4));
        $requestInsertionCorrectAnswer = $db->prepare("UPDATE quiz_successRate SET correctAnswers = correctAnswers + 1 WHERE idQuestion = :idQuestion");
        $requestInsertionWrongAnswer = $db->prepare("UPDATE quiz_successRate SET wrongAnswers = wrongAnswers + 1 WHERE idQuestion = :idQuestion");
        if (responses[$info-1] == $response) {
            $correct = true;
            $requestInsertionCorrectAnswer->execute(Array('idQuestion' => $info));
        }
        else {
            $correct = false;
            $requestInsertionWrongAnswer->execute(Array('idQuestion' => $info));
        }
        $requestSuccessRate = $db->prepare("SELECT (correctAnswers / (wrongAnswers+correctAnswers) * 100) FROM quiz_successRate WHERE idQuestion = :idQuestion");
        $requestSuccessRate->execute(Array('idQuestion' => $info));
        $successRate = $requestSuccessRate->fetch();
        Flight::json(array('correct' => $correct, 'successRate' => number_format($successRate[0],2)));
    }
    else { // In this case, @info contains the final score
        $requestInsertionScore = $db->prepare("INSERT INTO quiz_finalScore VALUES (NULL, :score)");
        $requestInsertionScore->execute(Array('score' => $info));
        $id = $db->lastInsertId();
        $requestRank = $db->query("SELECT FIND_IN_SET(score, (SELECT GROUP_CONCAT(score ORDER BY score DESC) FROM quiz_finalScore)) AS rank FROM quiz_finalScore WHERE idScore = $id");
        $rank = $requestRank->fetch();
        $requestRankingSize = $db->query("SELECT COUNT(idScore) FROM quiz_finalScore");
        $rankingSize = $requestRankingSize->fetch();
        Flight::json(array('rank' => $rank[0], 'rankingSize' => $rankingSize[0]));
    }
});

Flight::route('/works/quiz-iut/presentation', function(){
    Flight::render('works/quiz-iut/quiz-iut_presentation.tpl', Array('NAVBAR'=>NAVBAR, 'QUIZ_IUT'=>QUIZ_IUT, 'lang'=>$_SESSION['lang']));
});