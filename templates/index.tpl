﻿<!DOCTYPE html>
<html lang="{$lang}">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{$INDEX.META_DESCRIPTION}" />
    <meta name="author" content="Benoit CHEVILLON" />
    <link rel="canonical" href="https://www.benoit-chevillon.fr" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        ul {
            display: inline-block;
            text-align: center;
        }
        li {
            text-align: left;
            list-style-type: none;
        }
        h2 {
            font-weight: 600;
        }
        h3 {
            text-transform: uppercase;
            font-size: 1.3em;
        }
        #identity {
            margin: 0 auto;
            width: 230px;
        }
        #presentation {
            margin: 0 auto;
        }
        .langs {
            display: inline-block;
            padding: 8px 10px;
            border-width: 3px !important; 
        }
        #education svg {
            fill: #3c9cde;
        }
        @media (max-width:879px) {
            #skills
            {
                background-color: white !important;
                border-left: none !important;
            }
            #container-education-skills {
                background-color: white !important;
            }
            #interests {
                background-color: white !important;
            }
        }
        @media (min-width:880px) {
            #skills
            {
                border-top: none !important;
            }
            #experiences
            {
                background-color: white !important;
            }
        }
    </style>
    <title>CV & portfolio · Benoit CHEVILLON</title>
</head>
<body>
    {include file='header.tpl'}
    <main>
        <section class="container-fluid text-center">
            <section class="pb-3 pt-2 row">
                <div class="" id="identity">
                    <img alt="{$INDEX.IMG}" src="images/identity.jpg" class="img-thumbnail" />
                </div>
                <div class="" id="presentation">
                    <h1 class="center">Benoit <span class="text-primary">CHEVILLON</span></h1>
                    <p>
                        {$INDEX.YEARS} · 60134 Villers Saint Sépulcre · 06 52 70 98 21 ·
                        <a href="/contact">benoit.chevillon6@gmail.com</a>
                    </p>
                    <p class="lead">{$INDEX.PRESENTATION1}</p>
                    <p class="lead">{$INDEX.PRESENTATION2}</p>
                </div>
            </section>
            <hr class="m-0" />
            <section id="container-education-skills" class="bg-light pb-2">
                <section class="row justify-content-center" > 
                    <section id="education" class="bg-light col-sm p-0 pl-1 py-2 m-0" style="min-width: 60%; max-width: 100%;">
                        <h2>{$INDEX.EDUCATION}</h2>
                        <section>
                            <h3>Université de Technologie de Compiègne</h3>
                            <p class="mb-3">Diplôme d'ingénieur - Génie Informatique · <span class="text-primary">2021 - 2024</span></p>
                            <p class="m-0">{$INDEX.EDUCATION_UTC}</p>
                            <p>
                                {$INDEX.EDUCATION_UTC_RANKING1}
                                <a href="https://www.utc.fr/utc/lutc-en-bref/classements-et-chiffres-cles/">{$INDEX.EDUCATION_UTC_RANKING2}</a>{$INDEX.EDUCATION_UTC_RANKING3}
                            </p>
                        </section>
                        <section class="mt-5">
                            <h3>IUT d'Amiens</h3>
                            <p class="mb-3">DUT Informatique · <span class="text-primary">2019 - 2021</span></p>
                            <p class="m-0">{$INDEX.EDUCATION_IUT}</p>
                            <ul>
                                <li>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trophy-fill" viewBox="0 0 16 16">
                                        <path d="M2.5.5A.5.5 0 0 1 3 0h10a.5.5 0 0 1 .5.5c0 .538-.012 1.05-.034 1.536a3 3 0 1 1-1.133 5.89c-.79 1.865-1.878 2.777-2.833 3.011v2.173l1.425.356c.194.048.377.135.537.255L13.3 15.1a.5.5 0 0 1-.3.9H3a.5.5 0 0 1-.3-.9l1.838-1.379c.16-.12.343-.207.537-.255L6.5 13.11v-2.173c-.955-.234-2.043-1.146-2.833-3.012a3 3 0 1 1-1.132-5.89A33.076 33.076 0 0 1 2.5.5zm.099 2.54a2 2 0 0 0 .72 3.935c-.333-1.05-.588-2.346-.72-3.935zm10.083 3.935a2 2 0 0 0 .72-3.935c-.133 1.59-.388 2.885-.72 3.935z"/>
                                    </svg>
                                    {$INDEX.EDUCATION_IUT_LIST1}
                                </li>
                                <li>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trophy-fill" viewBox="0 0 16 16">
                                        <path d="M2.5.5A.5.5 0 0 1 3 0h10a.5.5 0 0 1 .5.5c0 .538-.012 1.05-.034 1.536a3 3 0 1 1-1.133 5.89c-.79 1.865-1.878 2.777-2.833 3.011v2.173l1.425.356c.194.048.377.135.537.255L13.3 15.1a.5.5 0 0 1-.3.9H3a.5.5 0 0 1-.3-.9l1.838-1.379c.16-.12.343-.207.537-.255L6.5 13.11v-2.173c-.955-.234-2.043-1.146-2.833-3.012a3 3 0 1 1-1.132-5.89A33.076 33.076 0 0 1 2.5.5zm.099 2.54a2 2 0 0 0 .72 3.935c-.333-1.05-.588-2.346-.72-3.935zm10.083 3.935a2 2 0 0 0 .72-3.935c-.133 1.59-.388 2.885-.72 3.935z"/>
                                    </svg>
                                    {$INDEX.EDUCATION_IUT_LIST2}
                                </li>
                                <li>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trophy-fill" viewBox="0 0 16 16">
                                        <path d="M2.5.5A.5.5 0 0 1 3 0h10a.5.5 0 0 1 .5.5c0 .538-.012 1.05-.034 1.536a3 3 0 1 1-1.133 5.89c-.79 1.865-1.878 2.777-2.833 3.011v2.173l1.425.356c.194.048.377.135.537.255L13.3 15.1a.5.5 0 0 1-.3.9H3a.5.5 0 0 1-.3-.9l1.838-1.379c.16-.12.343-.207.537-.255L6.5 13.11v-2.173c-.955-.234-2.043-1.146-2.833-3.012a3 3 0 1 1-1.132-5.89A33.076 33.076 0 0 1 2.5.5zm.099 2.54a2 2 0 0 0 .72 3.935c-.333-1.05-.588-2.346-.72-3.935zm10.083 3.935a2 2 0 0 0 .72-3.935c-.133 1.59-.388 2.885-.72 3.935z"/>
                                    </svg>
                                    {$INDEX.EDUCATION_IUT_LIST3}
                                </li>
                                <li>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trophy-fill" viewBox="0 0 16 16">
                                        <path d="M2.5.5A.5.5 0 0 1 3 0h10a.5.5 0 0 1 .5.5c0 .538-.012 1.05-.034 1.536a3 3 0 1 1-1.133 5.89c-.79 1.865-1.878 2.777-2.833 3.011v2.173l1.425.356c.194.048.377.135.537.255L13.3 15.1a.5.5 0 0 1-.3.9H3a.5.5 0 0 1-.3-.9l1.838-1.379c.16-.12.343-.207.537-.255L6.5 13.11v-2.173c-.955-.234-2.043-1.146-2.833-3.012a3 3 0 1 1-1.132-5.89A33.076 33.076 0 0 1 2.5.5zm.099 2.54a2 2 0 0 0 .72 3.935c-.333-1.05-.588-2.346-.72-3.935zm10.083 3.935a2 2 0 0 0 .72-3.935c-.133 1.59-.388 2.885-.72 3.935z"/>
                                    </svg>
                                    {$INDEX.EDUCATION_IUT_LIST4}
                                </li>
                            </ul>
                        </section>
                        <section class="mt-5">
                            <h3>Lycée Paul Langevin, Beauvais</h3>
                            <p class="mb-3">Baccalauréat scientifique · <span class="text-primary">2016 - 2019</span></p>
                            <p class="m-0">{$INDEX.EDUCATION_LANGEVIN}</p>
                        </section>
                    </section>
                    <section id="skills" class="col mb-2 mx-3 pt-2 border-left border-top" style="min-width: 320px;">
                        <h2>{$INDEX.SKILLS}</h2>
                        <ul class="list-group mx-auto" style="max-width: 600px;">
                            <li class="list-group-item py-2 active">
                                {$INDEX.SKILLS_COMPUTERS}
                            </li>
                            <li class="list-group-item py-2">
                                {$INDEX.SKILLS_COMPUTERS_LIST1}
                                <div class="progress mr-2">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </li>
                            <li class="list-group-item py-2">
                                {$INDEX.SKILLS_COMPUTERS_LIST2}
                                <div class="progress mr-2">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </li>
                            <li class="list-group-item py-2">
                                {$INDEX.SKILLS_COMPUTERS_LIST3}
                                <div class="progress mr-2">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </li>
                            <li class="list-group-item py-2">
                                {$INDEX.SKILLS_COMPUTERS_LIST4}
                                <div class="progress mr-2 py-0">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </li>
                            <li class="list-group-item py-2">
                                {$INDEX.SKILLS_COMPUTERS_LIST5}
                                <div class="progress mr-2">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </li>
                            <li class="list-group-item py-2">
                                {$INDEX.SKILLS_COMPUTERS_LIST6}
                                <div class="progress mr-2">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </li>
                            <li class="list-group-item py-2">
                                {$INDEX.SKILLS_COMPUTERS_LIST7}
                                <div class="progress mr-2">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </li>
                            <li class="list-group-item py-2">
                                {$INDEX.SKILLS_COMPUTERS_LIST8}
                               <div class="progress mr-2">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </li>
                            <li class="list-group-item py-2">
                                {$INDEX.SKILLS_COMPUTERS_LIST9}
                               <div class="progress mr-2">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </li>
                        </ul>
                        <ul class="list-group mt-3 mx-auto" style="max-width: 600px;">
                            <li class="list-group-item py-2 active">
                                {$INDEX.SKILLS_LANGUAGES}
                            </li>
                            <li class="list-group-item list-group-item d-flex justify-content-between align-items-center py-2">
                                {$INDEX.SKILLS_LANGUAGES_LIST1}
                                <span class="badge badge-primary badge-pill">B2</span>
                            </li>
                            <li class="list-group-item list-group-item d-flex justify-content-between align-items-center py-2">
                                {$INDEX.SKILLS_LANGUAGES_LIST2}
                                <span class="badge badge-primary badge-pill">~A2</span>
                            </li>
                        </ul>
                    </section>
                </section>
                <a href="/works" class="btn btn-primary">{$INDEX.SKILLS_WORK}</a>
            </section>
            <hr class="m-0" />
            <section id="experiences" class="bg-light py-2">
                <h2>{$INDEX.EXPERIENCE}</h2>
                <section>
                    <h3 class="mb-0">{$INDEX.EXPERIENCE_IDENTIBAT}</h3>
                    <p class="mb-3">IDENTIBAT, {$INDEX.EXPERIENCE_IDENTIBAT_DEVELOPMENT}, Saint-Quentin · <span class="text-primary">{$INDEX.EXPERIENCE_IDENTIBAT_DATE}</span></p>
                    <p>{$INDEX.EXPERIENCE_IDENTIBAT_PRESENTATION1}</p>
                </section>
                <section>
                    <h3 class="mb-0">{$INDEX.EXPERIENCE_INIGO}</h3>
                    <p class="mb-3">Inigo Factory, {$INDEX.EXPERIENCE_INIGO_DEVELOPMENT}, Paris · <span class="text-primary">{$INDEX.EXPERIENCE_INIGO_DATE}</span></p>
                    <p class="mb-0">{$INDEX.EXPERIENCE_INIGO_PRESENTATION1}</p>
                    <p>{$INDEX.EXPERIENCE_INIGO_PRESENTATION2}</p>
                </section>
                <section class="mb-2">
                    <h3 class="mb-0">{$INDEX.EXPERIENCE_MINIBUSINESS}</h3>
                    <p class="mb-3">SEGAMI, {$INDEX.EXPERIENCE_MINIBUSINESS_MARKETING}, Noailles · <span class="text-primary">2015 - 2016</span></p>
                    <p class="m-0">{$INDEX.EXPERIENCE_MINIBUSINESS_PRESENTATION1}</p>
                    <p class="m-0">{$INDEX.EXPERIENCE_MINIBUSINESS_PRESENTATION2}</p>
                    <p>{$INDEX.EXPERIENCE_MINIBUSINESS_PRESENTATION3}</p>
                </section>
            </section>
            <hr class="m-0" />
            <section id="interests" class="bg-light py-2">
                <h2 class="mb-3">{$INDEX.INTERESTS}</h2>
                <p class="mb-0">{$INDEX.INTERESTS_PRESENTATION1}</p>
                <p class="mb-0">{$INDEX.INTERESTS_PRESENTATION2}</p>
                <p class="mb-0">{$INDEX.INTERESTS_PRESENTATION3}</p>
            </section>
        </section>
    </main>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>