<!DOCTYPE html>
<html lang="{$lang}">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{$OSBOSE_GAME.META_DESCRIPTION}" />
    <meta name="author" content="Benoit CHEVILLON" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        h1 {
            font-size: 1.8em;
        }
        h2 {
            font-size: 1.5em;
        }
        .langs {
            display: inline-block;
            padding: 8px 10px;
            border-width: 3px !important;
        }
        section:first-child {
            max-width: 120ch;
            min-height: 80vh;
        }
        body {
            background-color: #f8f9fa;
        }
        .carousel {
            border: 1px solid #d6d6d6;
        }
    </style>
    <title>{$OSBOSE_GAME.PAGE_TITLE} · Benoit CHEVILLON</title>
</head>
<body>
{include file='header.tpl'}
<main class="bg-light pl-3 pr-3 pb-3">
    <section class="container-fluid text-center bg-white mt-2 pt-2 border border-muted">
        <h1>{$OSBOSE_GAME.MAIN_TITLE}</h1>
        <div id="carouselPresentation" class="carousel slide mt-2 mb-3" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselPresentation" data-slide-to="0" class="active"></li>
                <li data-target="#carouselPresentation" data-slide-to="1"></li>
                <li data-target="#carouselPresentation" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="/images/works/osbose-game/osbose-game_slide1" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/images/works/osbose-game/osbose-game_slide2" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/images/works/osbose-game/osbose-game_slide3" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselPresentation" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselPresentation" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <section class="text-left">
            <h2 class="text-center">{$OSBOSE_GAME.GENERAL}</h2>
            <p>{$OSBOSE_GAME.GENERAL_INTRO1}<span class="text-primary">2022</span>{$OSBOSE_GAME.GENERAL_INTRO2}<span class="text-primary">IC06 Industrie et conception des jeux vidéo</span>{$OSBOSE_GAME.GENERAL_INTRO3}</p>
            <p class="mb-0">{$OSBOSE_GAME.GENERAL_MODE}</p>
            <p class="mb-0">{$OSBOSE_GAME.GENERAL_PLAYERS}</p>
            <p>{$OSBOSE_GAME.GENERAL_CHARACTERS}</p>
            <p>{$OSBOSE_GAME.GENERAL_LEVELS}</p>
            <p>{$OSBOSE_GAME.GENERAL_MUSICS}</p>
            <p>{$OSBOSE_GAME.GENERAL_ANIMATIONS}</p>
        </section>
        <section class="text-left">
            <h2 class="text-center">{$OSBOSE_GAME.TECHNICAL}</h2>
            <p>{$OSBOSE_GAME.TECHNICAL_OBJECTIVE}</p>
            <p class="mb-0">{$OSBOSE_GAME.TECHNICAL_ENGINE1}<span class="text-primary">Godot</span>{$OSBOSE_GAME.TECHNICAL_ENGINE2}</p>
            <p>{$OSBOSE_GAME.TECHNICAL_TEMPLATE}</p>
            <p>{$OSBOSE_GAME.TECHNICAL_DIFFICULTY}</p>
        </section>
    </section>
</main>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>