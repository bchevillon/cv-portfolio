<!DOCTYPE html>
<html lang="{$lang}">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{$PHP_SCENARI_OPALE.META_DESCRIPTION}" />
    <meta name="author" content="Benoit CHEVILLON" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        h1 {
            font-size: 1.8em;
        }
        h2 {
            font-size: 1.5em;
        }
        .langs {
            display: inline-block;
            padding: 8px 10px;
            border-width: 3px !important;
        }
        section:first-child {
            max-width: 120ch;
            min-height: 80vh;
        }
        body {
            background-color: #f8f9fa;
        }
        .carousel {
            border: 1px solid #d6d6d6;
        }
    </style>
    <title>{$PHP_SCENARI_OPALE.PAGE_TITLE} · Benoit CHEVILLON</title>
</head>
<body>
{include file='header.tpl'}
<main class="bg-light pl-3 pr-3 pb-3">
    <section class="container-fluid text-center bg-white mt-2 pt-2 border border-muted">
        <h1>{$PHP_SCENARI_OPALE.MAIN_TITLE}</h1>
        <span class="badge badge-info">PHP</span>
        <span class="badge badge-dark">XML</span>
        <div id="carouselPresentation" class="carousel slide mt-2 mb-3" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselPresentation" data-slide-to="0" class="active"></li>
                <li data-target="#carouselPresentation" data-slide-to="1"></li>
                <li data-target="#carouselPresentation" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="/images/works/php-scenari-opale/php-scenari-opale_slide1" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/images/works/php-scenari-opale/php-scenari-opale_slide2" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/images/works/php-scenari-opale/php-scenari-opale_slide3" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselPresentation" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselPresentation" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <section class="text-left">
            <h2 class="text-center">{$PHP_SCENARI_OPALE.GENERAL}</h2>
            <p>{$PHP_SCENARI_OPALE.GENERAL_INTRO1}<span class="text-primary">2022</span>{$PHP_SCENARI_OPALE.GENERAL_INTRO2}<span class="text-primary">API050 Ingénierie des documents numériques pour la pédagogie</span>{$PHP_SCENARI_OPALE.GENERAL_INTRO3}</p>
            <p>{$PHP_SCENARI_OPALE.GENERAL_COURSE}</p>
            <p>{$PHP_SCENARI_OPALE.GENERAL_APPLICATION}</p>
            <p>{$PHP_SCENARI_OPALE.GENERAL_FORMATION}</p>
            <p>{$PHP_SCENARI_OPALE.GENERAL_LICENCE1}<a href="https://creativecommons.org/publicdomain/zero/1.0/deed.fr">Creative Commons Zero</a>{$PHP_SCENARI_OPALE.GENERAL_LICENCE2}</p>
        </section>
        <section class="text-left">
            <h2 class="text-center">{$PHP_SCENARI_OPALE.TECHNICAL}</h2>
            <p>{$PHP_SCENARI_OPALE.TECHNICAL_SCENARI1}<span class="text-primary">Scenari</span>{$PHP_SCENARI_OPALE.TECHNICAL_SCENARI2}<span class="text-primary">Opale</span>{$PHP_SCENARI_OPALE.TECHNICAL_SCENARI3}<span class="text-primary">Sunrise</span>{$PHP_SCENARI_OPALE.TECHNICAL_SCENARI4}</p>
            <p>{$PHP_SCENARI_OPALE.TECHNICAL_FEATURES}</p>
            <p>{$PHP_SCENARI_OPALE.TECHNICAL_XML1}<span class="text-primary">XML</span>{$PHP_SCENARI_OPALE.TECHNICAL_XML2}</p>
        </section>
    </section>
</main>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>
