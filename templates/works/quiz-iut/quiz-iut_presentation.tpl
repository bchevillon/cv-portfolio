<!DOCTYPE html>
<html lang="{$lang}">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{$QUIZ_IUT.META_DESCRIPTION}" />
    <meta name="author" content="Benoit CHEVILLON" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        h1 {
            font-size: 1.8em;
        }
        h2 {
            font-size: 1.5em;
        }
        .langs {
            display: inline-block;
            padding: 8px 10px;
            border-width: 3px !important;
        }
        section:first-child {
            max-width: 120ch;
            min-height: 80vh;
        }
        body {
            background-color: #f8f9fa;
        }
    </style>
    <title>Quiz IUT/BUT · Benoit CHEVILLON</title>
</head>
<body>
{include file='header.tpl'}
<main class="bg-light pl-3 pr-3 pb-3">
    <section class="container-fluid text-center bg-white mt-2 pt-2 border border-muted">
        <h1>{$QUIZ_IUT.MAIN_TITLE}</h1>
        <span class="badge badge-primary">HTML/CSS</span>
        <span class="badge badge-warning">JS</span>
        <span class="badge badge-info">PHP</span>
        <span class="badge badge-light">SQL</span>
        <div id="carouselPresentation" class="carousel slide mt-2 mb-3" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselPresentation" data-slide-to="0" class="active"></li>
                <li data-target="#carouselPresentation" data-slide-to="1"></li>
                <li data-target="#carouselPresentation" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="/images/works/quiz-iut/quiz-iut_slide1" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/images/works/quiz-iut/quiz-iut_slide2" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/images/works/quiz-iut/quiz-iut_slide3" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselPresentation" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselPresentation" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <section class="text-left">
            <h2 class="text-center">{$QUIZ_IUT.GENERAL}</h2>
            <p>{$QUIZ_IUT.GENERAL_INTRO1}<span class="text-primary">2021</span>{$QUIZ_IUT.GENERAL_INTRO2}<span class="text-primary">M4203 Expression-Communication</span>{$QUIZ_IUT.GENERAL_INTRO3}</p>
            <p class="mb-0">{$QUIZ_IUT.GENERAL_OBJECTIVE}</p>
            <p>{$QUIZ_IUT.GENERAL_IUT}</p>
            <p class="mb-0">{$QUIZ_IUT.GENERAL_TARGET}</p>
            <ul>
                <li>{$QUIZ_IUT.GENERAL_TARGET_LIST1}</li>
                <li>{$QUIZ_IUT.GENERAL_TARGET_LIST2}</li>
                <li>{$QUIZ_IUT.GENERAL_TARGET_LIST3}</li>
            </ul>
            <p>{$QUIZ_IUT.GENERAL_LANG}</p>
            <p>{$QUIZ_IUT.GENERAL_PLAYFUL}</p>
            <p>{$QUIZ_IUT.GENERAL_EXPLANATION}</p>
            <p>{$QUIZ_IUT.GENERAL_CUSTOM}</p>
        </section>
        <section class="text-left">
            <h2 class="text-center">{$QUIZ_IUT.TECHNICAL}</h2>
            <p>{$QUIZ_IUT.TECHNICAL_JAVASCRIPT1}<span class="text-primary">Javascript</span>{$QUIZ_IUT.TECHNICAL_JAVASCRIPT2}<span class="text-primary">JQuery</span>{$QUIZ_IUT.TECHNICAL_JAVASCRIPT3}</p>
            <p>{$QUIZ_IUT.TECHNICAL_INTERFACES}</p>
            <p class="mb-0">{$QUIZ_IUT.TECHNICAL_API1}<span class="text-primary">PHP</span>{$QUIZ_IUT.TECHNICAL_API2}</p>
            <p>{$QUIZ_IUT.TECHNICAL_AJAX}</p>
            <p>{$QUIZ_IUT.TECHNICAL_DATABASE1}<span class="text-primary">SQL</span>{$QUIZ_IUT.TECHNICAL_DATABASE2}<span class="text-primary">PDO</span>{$QUIZ_IUT.TECHNICAL_DATABASE3}<span class="text-primary">MySQL</span>{$QUIZ_IUT.TECHNICAL_DATABASE4}</p>
        </section>
    </section>
</main>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>