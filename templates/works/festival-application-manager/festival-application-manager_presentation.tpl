<!DOCTYPE html>
<html lang="{$lang}">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{$FESTIVAL_APPLICATION_MANAGER.META_DESCRIPTION}" />
    <meta name="author" content="Benoit CHEVILLON" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        h1 {
            font-size: 1.8em;
        }
        h2 {
            font-size: 1.5em;
        }
        .langs {
            display: inline-block;
            padding: 8px 10px;
            border-width: 3px !important;
        }
        section:first-child {
            max-width: 120ch;
            min-height: 80vh;
        }
        body {
            background-color: #f8f9fa;
        }
        .carousel {
            border: 1px solid #d6d6d6;
        }
    </style>
    <title>{$FESTIVAL_APPLICATION_MANAGER.PAGE_TITLE} · Benoit CHEVILLON</title>
</head>
<body>
{include file='header.tpl'}
<main class="bg-light pl-3 pr-3 pb-3">
    <section class="container-fluid text-center bg-white mt-2 pt-2 border border-muted">
        <h1>{$FESTIVAL_APPLICATION_MANAGER.MAIN_TITLE}</h1>
        <span class="badge badge-primary">HTML/CSS</span>
        <span class="badge badge-warning">JS</span>
        <span class="badge badge-info">PHP</span>
        <span class="badge badge-light">SQL</span>
        <div id="carouselPresentation" class="carousel slide mt-2 mb-3" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselPresentation" data-slide-to="0" class="active"></li>
                <li data-target="#carouselPresentation" data-slide-to="1"></li>
                <li data-target="#carouselPresentation" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="/images/works/festival-application-manager/festival-application-manager_slide1" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/images/works/festival-application-manager/festival-application-manager_slide2" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/images/works/festival-application-manager/festival-application-manager_slide3" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselPresentation" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselPresentation" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <section class="text-left">
            <h2 class="text-center">{$FESTIVAL_APPLICATION_MANAGER.GENERAL}</h2>
            <p>{$FESTIVAL_APPLICATION_MANAGER.GENERAL_INTRO1}<span class="text-primary">2020</span>{$FESTIVAL_APPLICATION_MANAGER.GENERAL_INTRO2}<span class="text-primary">M3104 Programmation web côté serveur</span>{$FESTIVAL_APPLICATION_MANAGER.GENERAL_INTRO3}</p>
            <p class="mb-0">{$FESTIVAL_APPLICATION_MANAGER.GENERAL_MANAGER}</p>
            <p>{$FESTIVAL_APPLICATION_MANAGER.GENERAL_ERRORS}</p>
            <p class="mb-0">{$FESTIVAL_APPLICATION_MANAGER.GENERAL_ADMIN1}</p>
            <p>{$FESTIVAL_APPLICATION_MANAGER.GENERAL_ADMIN2}</p>
            <p class="mb-0">{$FESTIVAL_APPLICATION_MANAGER.GENERAL_ROUTES}</p>
            <ul>
                <li>/stats/nombre-candidatures</li>
                <li>/stats/candidatures-par-departement</li>
                <li>/stats/candidatures-par-departement/(numDep)</li>
                <li>/stats/candidatures-hauts-de-France</li>
                <li>/stats/candidatures-hors-hauts-de-France</li>
                <li>/stats/candidatures-par-scene</li>
            </ul>
        </section>
        <section class="text-left">
            <h2 class="text-center">{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL}</h2>
            <p class="mb-0">{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_PHP1}<span class="text-primary">PHP</span>{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_PHP2}</p>
            <p>{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_CLIENT1}<span class="text-primary">Javascript</span>{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_CLIENT2}<span class="text-primary">HTML/CSS</span>{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_CLIENT3}<span class="text-primary">Bootstrap</span>{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_CLIENT4}</p>
            <p class="mb-0">{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_FRAMEWORK1}<span class="text-primary">Flight</span>{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_FRAMEWORK2}</p>
            <p>{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_TEMPLATE1}<span class="text-primary">Smarty</span>{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_TEMPLATE2}</p>
            <p>{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_VERIFICATIONS}</p>
            <p class="mb-0">{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_DATABASE1}<span class="text-primary">MySQL</span>{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_DATABASE2}<span class="text-primary">PDO</span>{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_DATABASE3}<span class="text-primary">SQL</span>{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_DATABASE4}</p>
            <p>{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_RETRIEVE}</p>
            <p>{$FESTIVAL_APPLICATION_MANAGER.TECHNICAL_API}</p>
        </section>
    </section>
</main>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>