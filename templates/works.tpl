<!DOCTYPE html>
<html lang="{$lang}">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{$WORKS.META_DESCRIPTION}" />
    <meta name="author" content="Benoit CHEVILLON" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        h1 {
            font-size: 1.8em;
        }
        h2 {
            font-size: 1.5em;
        }
        .langs {
            display: inline-block;
            padding: 8px 10px;
            border-width: 3px !important;
        }
        .card {
            height: 475px !important;
        }
    </style>
    <title>{$WORKS.PAGE_TITLE} · Benoit CHEVILLON</title>
</head>
<body>
{include file='header.tpl'}
<main>
    <section class="container-fluid text-center">
        <h1>{$WORKS.MAIN_TITLE}</h1>
        <section class="row">
            {foreach from=$worksList item=work}
            <div class="m-auto">
                <div class="card m-2" style="width: 18rem">
                    <img class="card-img-top" src="../images/works/{$work->image}" alt="{if $lang == FR}{$work->titleFR|replace:'"':''}{else}{$work->titleEN|replace:'"':''}{/if}">
                    <div class="card-body pt-0">
                        {foreach from=$work->labels key=labelTitle item=labelColor}
                        <span class="badge badge-{$labelColor}">{$labelTitle}</span>
                        {/foreach}
                        <h2 class="card-title">{if $lang == FR}{$work->titleFR}{else}{$work->titleEN}{/if}</h2>
                        <p class="card-text">{if $lang == FR}{$work->presentationFR}{else}{$work->presentationEN}{/if}</p>
                        {if $work->urlWork != ""}
                        <a href="{$work->urlWork}" class="btn btn-primary">{$WORKS.CARD_GO}</a>
                        {/if}
                        <a href="/works/{$work->urlPresentation}" class="btn btn-primary">{$WORKS.CARD_PRESENTATION}</a>
                    </div>
                </div>
            </div>
            {/foreach}
        </section>
    </section>
</main>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>