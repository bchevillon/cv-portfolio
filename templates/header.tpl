<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="/">Benoit CHEVILLON</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto">
                {if $smarty.server.REQUEST_URI == "/"}
                <li class="nav-item active">
                    <a class="nav-link" href="/">{$NAVBAR.HOME}<span class="sr-only">(current)</span></a>
                </li>
                {else}
                <li class="nav-item">
                    <a class="nav-link" href="/">{$NAVBAR.HOME}</a>
                </li>
                {/if}

                {if $smarty.server.REQUEST_URI == "/works"}
                    <li class="nav-item active">
                        <a class="nav-link" href="/works">{$NAVBAR.WORKS}<span class="sr-only">(current)</span></a>
                    </li>
                {else}
                    <li class="nav-item">
                        <a class="nav-link" href="/works">{$NAVBAR.WORKS}</a>
                    </li>
                {/if}
            </ul>
            <a class="nav-link" href="https://gitlab.com/bchevillon">
                <svg width="26" height="26" class="tanuki-logo" viewBox="0 0 36 36">
                    <path class="tanuki-shapes tanuki-left-ear" fill="#e24329" d="M2 14l9.38 9v-9l-4-12.28c-.205-.632-1.176-.632-1.38 0z"></path>
                    <path class="tanuki-shapes tanuki-right-ear" fill="#e24329" d="M34 14l-9.38 9v-9l4-12.28c.205-.632 1.176-.632 1.38 0z"></path>
                    <path class="tanuki-shape tanuki-nose" fill="#e24329" d="M18,34.38 3,14 33,14 Z"></path>
                    <path class="tanuki-shape tanuki-left-eye" fill="#fc6d26" d="M18,34.38 11.38,14 2,14 6,25Z"></path>
                    <path class="tanuki-shape tanuki-right-eye" fill="#fc6d26" d="M18,34.38 24.62,14 34,14 30,25Z"></path>
                    <path class="tanuki-shape tanuki-left-cheek" fill="#fca326" d="M2 14L.1 20.16c-.18.565 0 1.2.5 1.56l17.42 12.66z"></path>
                    <path class="tanuki-shape tanuki-right-cheek" fill="#fca326" d="M34 14l1.9 6.16c.18.565 0 1.2-.5 1.56L18 34.38z"></path>
                </svg>
            </a>
            <a class="nav-link" href="https://www.linkedin.com/in/beno%C3%AEt-chevillon-4092b7194/">
                <svg width="26" height="26" fill="white" class="bi bi-linkedin" viewBox="0 0 16 16">
                    <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z" />
                </svg>
            </a>
            <span class="nav-link">
                <a class="text-white" href="/contact">{$NAVBAR.CONTACT}</a>
            </span>
        </div>
    </nav>
    <section class="bg-dark langs border-top border-primary">
        <div class="btn-group rounded-0" role="group">
            {if $smarty.server.REQUEST_URI == "/"}
            <a href="FR">
            {else}
            <a href="{$smarty.server.REQUEST_URI}/FR">
            {/if}
                <button type="button rounded-0" class="btn btn-primary {if $lang == FR}active{/if}">&#x1F1EB;&#x1F1F7;</button>
            </a>
            {if $smarty.server.REQUEST_URI == "/"}
            <a href="EN">
            {else}
            <a href="{$smarty.server.REQUEST_URI}/EN">
            {/if}
                <button type="button rounded-0" class="btn btn-primary {if $lang == EN}active{/if}">&#x1f1fa;&#x1f1f8;</button>
            </a>
        </div>
    </section>
</header>