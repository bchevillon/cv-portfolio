﻿<!DOCTYPE html>
<html lang="{$lang}">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{$CONTACT.META_DESCRIPTION}" />
    <meta name="author" content="Benoit CHEVILLON" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        h2 {
            font-weight: 600;
            font-size: 1.5em;
        }
        .langs {
            display: inline-block;
            padding: 8px 10px;
            border-width: 3px !important; 
        }
    </style>
    <title>Contact · Benoit CHEVILLON</title>
</head>
<body>
    {include file='header.tpl'}
    <main>
        <section class="container-fluid text-center">
            <div class="card text-white bg-info mb-3 mx-auto" style="max-width: 18rem;">
                <div class="card-header">{$CONTACT.INFOS_HEADER}</div>
                <div class="card-body">
                    <p class="card-text">📱 06 52 70 98 21</p>
                    <p class="card-text">📧 <a class="text-light" href="mailto:benoit.chevillon6@gmail.com">benoit.chevillon6@gmail.com</a></p>
                </div>
            </div>
            <section class="bg-light w-75 border p-2 mb-3 mx-auto" id="section-form">
                {if isset($send)}
                {if $send}
                <div class="alert alert-success mt-2" role="alert">
                    {$CONTACT.FORM_SUCCESS}
                </div>
                {else}
                <div class="alert alert-danger mt-2" role="alert">
                    {$CONTACT.FORM_ERROR}
                </div>
                {/if}
                {/if}
                <h2>{$CONTACT.FORM_TITLE}</h2>
                <form method="POST" action="">
                    <div class="form-row">
                        <div class="col-md-4 mb-3" id="div_name">
                            <label for="contact_name">{$CONTACT.FORM_NAME}</label>
                            <input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="{$CONTACT.FORM_NAME}" maxlength="75" required>
                            <div class="invalid-feedback">{$CONTACT.FORM_NAME_INVALID}</div>
                        </div>
                        <div class="col-md-4 mb-3" id="div_email">
                            <label for="contact_email">{$CONTACT.FORM_EMAIL}</label>
                            <input type="email" class="form-control" id="contact_email" name="contact_email" placeholder="{$CONTACT.FORM_EMAIL}" required>
                            <div class="invalid-feedback">{$CONTACT.FORM_EMAIL_INVALID}</div>
                        </div>
                        <div class="col-md-4 mb-3" id="div_subject">
                            <label for="contact_subject">{$CONTACT.FORM_SUBJECT}</label>
                            <input type="text" class="form-control" id="contact_subject" name="contact_subject" placeholder="{$CONTACT.FORM_SUBJECT}" maxlength="75">
                            <div class="invalid-feedback">{$CONTACT.FORM_SUBJECT_INVALID}</div>
                        </div>
                    </div>
                    <div class="div_message">
                        <label for="contact_message">{$CONTACT.FORM_MESSAGE}</label>
                        <textarea class="form-control" id="contact_message" name="contact_message" placeholder="{$CONTACT.FORM_MESSAGE}" maxlength="5000" style="height: 300px;" required></textarea>
                        <div class="invalid-feedback">{$CONTACT.FORM_MESSAGE_INVALID}</div>
                    </div>
                    <input type="hidden" id="recaptcha_response" name="recaptcha_response">
                    <button type="submit" class="btn btn-primary mt-2 g-recaptcha" id="contact_submitButton"
                            data-sitekey="6Lc19DAcAAAAAHilBD1HcxWatpRaMaKAq9epqnjG"
                            data-callback='onSubmit'
                            data-action='contact'>{$CONTACT.FORM_VALIDATION}</button>
                </form>
            </section>
        </section>
    </main>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
        $(function(){
            $('#contact_name').blur(vName)
            $('#contact_email').blur(vEmail)
            $('#contact_subject').blur(vSubject)
            $('#contact_message').blur(vMessage)
        })

        function vName() {
            if (!$('#contact_name').prop("validity").valid) {
                $('#div_name .invalid-feedback').show()
                $('#contact_name').addClass('is-invalid')
                return false
            }
            else {
                $('#div_name .invalid-feedback').hide()
                $('#contact_name').removeClass('is-invalid')
                return true
            }
        }

        function vEmail() {
            if (!$('#contact_email').prop("validity").valid) {
                $('#div_email .invalid-feedback').show()
                $('#contact_email').addClass('is-invalid')
                return false
            }
            else {
                $('#div_email .invalid-feedback').hide()
                $('#contact_email').removeClass('is-invalid')
                return true
            }
        }

        function vSubject() {
            if (!$('#contact_subject').prop("validity").valid) {
                $('#div_subject .invalid-feedback').show()
                $('#contact_subject').addClass('is-invalid')
                return false
            }
            else {
                $('#div_subject .invalid-feedback').hide()
                $('#contact_subject').removeClass('is-invalid')
                return true
            }
        }

        function vMessage() {
            if (!$('#contact_message').prop("validity").valid) {
                $('#div_message .invalid-feedback').show()
                $('#contact_message').addClass('is-invalid')
                return false
            }
            else {
                $('#div_message .invalid-feedback').hide()
                $('#contact_message').removeClass('is-invalid')
                return true
            }
        }

        function onSubmit(token) {
            if (vName() & vEmail() & vSubject() & vMessage()) {
                $("#recaptcha_response").val(token);
                $('form').submit();
            }
        }
    </script>
</body>
</html>