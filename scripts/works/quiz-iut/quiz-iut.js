const questionList = [
    "Quelle nouvelle formation “modifie” le DUT, dès 2021 ?",
    "Quelle est la durée de formation incorrecte ?",
    "Les DUT et BUT sont réalisés dans des :",
    "Laquelle de ces formations contient la part plus importante de pratique, par rapport à la théorie ?",
    "Quelle est la décomposition d’une année de BUT ?",
    "Dans un DUT ou dans un BUT, une période de stage est :",
    "Quelle est la durée minimale de stages nécessaire à l’obtention d’un BUT ?",
    "Quel est le format d’évaluation lors d’une formation à l’IUT ?",
    "Quelle est la nature des projets réalisés durant la formation ?",
    "Les DUT et les BUT permettent :",
    "Parmi celles-ci, quelle est la poursuite d'études directement impossible après avoir obtenu un BUT Informatique ?",
    "Combien existe-t-il d’IUT proposant un BUT Informatique en France ?",
    "Existe-t-il un coût d’inscription dans les formations d’une IUT ?",
    "Quel est le niveau de chômage touchant les diplômés de DUT/BUT Informatique ?",
    "Quels sont les types d’enseignants assurant des cours pendant les formations d’un IUT ?"
]
const responseList = [
    ["BUT", "BUD", "BTS"],
    ["DUT : 2 ans", "BUT : 3 ans", "CPGE (Classe préparatoire) : 2 ans", "Licence : 2 ans"],
    ["Lycées", "Ecoles d’ingénieurs", "IUT (Institut Universitaire Technologique)"],
    ["BUT", "Classe préparatoire", "Licence"],
    ["Aucune décomposition","2 semestres","3 trimestres"],
    ["Obligatoire", "Facultative"],
    ["De 8 à 18 semaines", "De 22 à 26 semaines", "De 30 à 36 semaines"],
    ["Contrôle continu", "Partiels à chaque fin de semestre", "Projets uniquements"],
    ["Projets uniquement théoriques", "Projets uniquement pratiques", "Projets parfois transdisciplinaires"],
    ["Une insertion dans le marché du travail uniquement", "Une poursuite d’étude uniquement", "Les 2 possibilités précédentes"],
    ["1ère année de master", "Formation en école d’ingénieurs", "Formation en école de commerce", "1ère année de doctorat"],
    ["10 établissements", "30 établissements", "32 établissements"],
    ["Toujours", "Oui, mais les étudiants boursiers sont exonérés", "Parfois, en fonction de la localisation", "Jamais"],
    ["Taux de chômage faible", "Taux de chômage moyen", "Taux de chômage élevée"],
    ["Enseignants titulaires", "Enseignants chercheurs", "Enseignants vacataires", "Chaque type d’enseignant ci-dessus"]
]
const explanationList = [
    "Dès 2021, le fonctionnement sera modifié en accueillant un nouveau diplôme, le BUT : Bachelor Universitaire de Technologie. Le DUT (Diplôme Universitaire de Technologie) ne sera pas entièrement supprimé mais sera intégré à la formation du BUT. Le BUT dure une année supplémentaire, il peut être qualifié de fusion du DUT et de la Licence Professionnelle. Même si cela est déconseillé, il est possible d’arrêter sa formation une année avant d’obtenir son BUT et obtenir uniquement un DUT.",
    "2 ans de formation sont nécessaires afin d’obtenir un DUT. Comme expliqué précédemment, le BUT dure 1 année supplémentaire, soit 3 ans. La classe préparatoire forme les étudiants pendant 2 ans, tout comme les BTS. 3 ans de formation sont nécessaires afin d’obtenir une licence.",
    "Les IUT (Institut Universitaire Technologique) sont des établissements accueillant les formations afin d’obtenir un DUT et un BUT, mais également une licence professionnelle. Un IUT est un institut interne d’une université publique.",
    "Parmi ces différentes propositions, le BUT est en effet la formation contenant la part de pratique la plus importante. La formation est divisée équitablement entre TD théorique et TP pratique. Dans certains cas, les TP ont lieu en groupes plus restreints. Les enseignements théoriques dominent lors d’une formation en licence ou en classe préparatoire.",
    "Un BUT ainsi que tout autre formation à l’IUT se déroule en 2 semestres, chaque année d’étude. Il s’agit majoritairement de septembre à janvier puis de janvier à juillet.",
    "Que ce soit le DUT ou le BUT, ces formations aident les étudiants à s’insérer dans le monde professionnel. Il est donc important qu’un stage soit obligatoire afin d’acquérir le diplôme.",
    "Réparties entre les stages des différents semestres, entre 22 et 26 semaines de stage sont nécessaires et obligatoires pour obtenir le BUT. Ces stages se réalisent en entreprise et ont pour objectif d’aider la future insertion dans le monde professionnel.",
    "Toute formation à l’IUT est évaluée sur la base du contrôle continu. Des notes peuvent donc être obtenues tout au long du semestre. Malgré cela, il est possible que des semaines types “partiels” soient effectuées, celles-ci comportent une majorité d’examens.",
    "De nombreux projets sont réalisés au cours de la formation, leur nombre augmente davantage par rapport à l’ancien DUT. Ces projets peuvent être centrés sur des modules précis mais également être transdisciplinaires pour permettre d’établir les relations entre les différents apprentissages.",
    "Comme expliqué précédemment, les DUT et BUT permettent une insertion dans le marché du travail grâce aux différents projets tutorés et aux différentes périodes de stage. Il est également entièrement possible de continuer ses études, 90% des étudiants ayant eu un DUT poursuivent leurs études.",
    "De très nombreuses poursuites d’études sont possibles après un BUT : 1ère année de master, école d’ingénieurs, école de commerce. Des reconversions sont également possibles en se redirigeant vers une formation post-BAC, ou parfois à un niveau BAC+1, BAC+2 ou BAC+3. L'accès à un doctorat (entrée en BAC+5 et diplôme délivré en BAC+7) est donc impossible.",
    "En France, il existe 30 IUT proposant un BUT Informatique. Répartis dans toute la France, ils permettent à chaque étudiant de suivre facilement ses études. Le nombre d'élèves et les spécialités proposées varient en fonction de chaque établissement.",
    "Il existe effectivement un coût d’inscription pour toute formation dans un IUT. Ce coût est identique au niveau national et fixé pour toutes les universités. Il s’élève à 170€ pour les étudiants français. Cependant, chaque étudiant boursier peut être exonéré de ces frais.",
    "En globalité, les possesseurs d’un DUT sont touchés entre 3 et 11% par le chômage. Cependant, les métiers et la technologie informatique sont en constante évolution. L’informatique est de plus en plus utilisée et le nombre d’offres d’emploi dans ce secteur est toujours croissant. Il s’agit donc de l’une des filières les moins touchées par le chômage.",
    "En plus des enseignants titulaires, présents en permanence à l’IUT, des enseignants vacataires dispensent des cours aux étudiants. Il s’agit de personnes insérées dans le monde du travail et souhaitant transmettre leur savoir et leurs méthodes. Des enseignants chercheurs sont également présents et travaillent dans des laboratoires de recherche liés à l’université. Cette variété permet une diversité des points de vue et est une force pour le BUT."
]
const adviceList = [
    "<ul><li><a href='https://www.onisep.fr/Choisir-mes-etudes/Apres-le-bac/Organisation-des-etudes-superieures/Les-BUT-bachelors-universitaires-de-technologie'>BUT - ONISEP</a></li><li><a href='https://but.iut.fr/'>BUT - IUT.fr</a></li><li><a href='https://www.iut.fr/formations-et-diplomes/pourquoi-s-inscrire-a-l-iut-.html'>Les IUT - IUT.fr</a></li></ul>",
    "Vous connaissez quasiment parfaitement le sujet ! Vous disposez de très bonnes bases pour choisir ou bien commencer votre formation. Si des informations supplémentaires sont nécessaires, nous vous conseillons ces différentes pages :",
    "Vous connaissez bien le sujet ! Vous disposez de bonnes bases pour choisir ou bien commencer votre formation, votre recherche est bien avancée. Afin d'obtenir les dernières informations manquantes, nous vous conseillons ces différentes pages :",
    "Vous connaissez moyennement le sujet. Le BUT ne vous est pas complètement inconnu mais des informations nécessaires vous sont encore manquantes. Ces informations sont indispensables pour bien choisir une formation, ou bien la débuter. Afin d'obtenir ces informations manquantes, nous vous conseillons ces différentes pages :",
    "Vous ne connaissez presque pas le sujet ! Vous ne disposez que de très peu d'informations. Vous devez absolument faire davantage de recherche afin de ne pas rater votre choix de formation, ou votre entrée dans un BUT. Afin de mieux vous préparer, nous vous conseillons ces différentes pages :",
]
let questionsNumber = 0
let score = 0
const success = new Audio('../audio/success.wav');
const error = new Audio('../audio/error.wav');
const whoosh = new Audio('../audio/whoosh.wav');


function questionChange() { // Display of the next question 
    questionsNumber++
    whoosh.pause()
    whoosh.currentTime = 0
    whoosh.play()
    $("main h1").prop("innerHTML", questionsNumber + ". " + questionList[questionsNumber - 1])
    $("#display").prop("innerHTML", "")
    let reponsesNumber = responseList[questionsNumber - 1].length;
    for(let reponseNum = 1; reponseNum <= reponsesNumber; reponseNum++) {
        let newResponse = $("<a href='#'></a>")
        newResponse.prop("innerHTML", "<div class='bg-light py-3 px-5 my-4  text-center round shadow vivify popInTop propositions reponse" + reponseNum
        + "'><h2 class='my-auto reponse" + reponseNum + "'>" + responseList[questionsNumber - 1][reponseNum - 1] + "</h2></div>")
        $("#display").append(newResponse)
    }
    $(".propositions" ).click(explanation)
}

function explanation() { // Display of explanation after answering 
    $("#display").prop("innerHTML", "")
    let successRate = $("<h2 class='p-3 text-center text-white vivify popInTop'></h2>")
    $("#display").append(successRate)
    $.getJSON("quiz-iut-api/" + questionsNumber + "/" + $(this).attr("class").split("reponse")[1], function(data) {
        if (data.correct) { // "AJAX" request returning if this is the correct answer and the sucess rate
            success.pause()
            success.currentTime = 0
            success.play()
            $("main h1").prop("innerHTML", "Bonne réponse !")
            score++
        }
        else {
            error.pause()
            error.currentTime = 0
            error.play()
            $("main h1").prop("innerHTML", "Mauvaise réponse !")
        }
        successRate.prop("innerHTML", "Taux de réussite moyen : " + data.successRate + "%")
    })
    let explanations = $("<p class='p-3 shadow vivify popInTop'></p>")
    explanations.prop("innerHTML", explanationList[questionsNumber - 1])
    $("#display").append(explanations)
    let button = $("<button class='btn btn-light d-block mx-auto'>Continuer ➞</button>")
    $("#display").append(button)
    if (questionsNumber < questionList.length) // If all questions have been asked
        $("button" ).click(questionChange)
    else
        $("button" ).click(results)
}

function results() {
    $("#display").prop("innerHTML", "")
    $("main h1").prop("innerHTML","Votre score : " + score + "/" + questionList.length)
    let rank = $("<h2 class='p-3 text-center text-white vivify popInTop'></h2>")
    $("#display").append(rank)
    $.getJSON("quiz-iut-api/" + score, function(data) { // "AJAX" request returning the rank
        rank.prop("innerHTML", "Classement : " + data.rank + "/" + data.rankingSize)
    })
    let adviceNumber
    let globalSuccessRate = score/questionList.length
    let successRateInterval = [0.9, 0.7, 0.4, 0]
    $.each(successRateInterval, function(index, value) {
            if (globalSuccessRate >= value) {
                adviceNumber = index + 1
                return false
            }
        })
    let advice = $("<p class='p-3 advices shadow vivify popInTop'></p>")
    advice.prop("innerHTML", adviceList[adviceNumber] + adviceList[0])
    $("#display").append(advice)
}

$(function() {
    let ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf('safari') != -1)
        if (ua.indexOf('chrome') == -1) // Safari is used
            $("#cubes").prop("innerHTML", "");
    $("button" ).click(questionChange)
    $("#display h2").prop("innerHTML", questionList.length + " questions - Votre score sera enregistré !")
})