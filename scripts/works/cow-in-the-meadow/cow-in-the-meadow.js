/**
 * Initialisation d'événements/calculs de démarrage, exécuté dès le chargement de jQuery.
 */
$(() => {
    $("#resultat").hide()
    // Recalcul nombre de cases au début si jamais nb de piquets déjà défini (cache)
    actualisationInputs()
    // Lors d'une modification du nb de piquets, recalcul nb de cases
    $("#nbPiquets").on("input",(() => {actualisationInputs(false)}))
    // Lors d'une perte de focus sur la case, recalcul en vérifiant si < 3
    $("#nbPiquets").blur(() => {actualisationInputs(true)})
})

/**
 * Défini le bon nombre de cases input en fonction du nombre de piquets
 * @param {boolean} verificationMin - Indique si le minimum de piquets est a vérifié
 */
function actualisationInputs(verificationMin) {
    let nbPiquets = $("#nbPiquets").val()
    nbPiquets = verificationNbPiquets(verificationMin, nbPiquets)
    let nbPiquetsActuels = $(".piquet").length // Nombre de cases de piquets actuellement affichées

    ajoutInput(nbPiquets,nbPiquetsActuels) // Ajout eventuel de cases
    suppressionInput(nbPiquets,nbPiquetsActuels) // Retrait eventuel de cases
    actualisationEventsInputs() // Actualisation des evenements de verification de saisi
}

/**
 * Vérifie si le nombre de piquets saisi est correct, le modifie sinon
 * @param {boolean} verificationMin - Indique si le minimum de piquet est a vérifié
 * @param {number} nbPiquets - Nombre de piquets total
 * @return {number} Nombre de piquets après vérification et éventuelle modification
 */
function verificationNbPiquets(verificationMin, nbPiquets) {
    let nouveauNbPiquets = nbPiquets;

    // Si vérif de minimum, si pas un nombre ou < 3, on force à 3
    if (verificationMin && ((nbPiquets < 3) || (!$.isNumeric(nbPiquets))))
        nouveauNbPiquets = 3
    if (nbPiquets > 50) // Si > 50, on force à 50
        nouveauNbPiquets = 50
    if (nbPiquets !== nouveauNbPiquets)
        $("#nbPiquets").val(nouveauNbPiquets) // On écrit la valeur forcée
    return nouveauNbPiquets
}

/**
 * Ajoute des cases input si nécessaire
 * @param {number} nbPiquets - Nombre de piquets demandé
 * @param {number} nbPiquetsActuels - Nombre de piquets avant modification
 */
function ajoutInput(nbPiquets, nbPiquetsActuels) {
    for (let i = nbPiquetsActuels; i < nbPiquets; i++) // Pour chaque piquet
        $("#inputs").append( // Ajout d'une paire de cases coordonnée X/Y
            "<div class='d-flex justify-content-around mt-2 piquet'>" +
            "<label class='align-self-center text-center' " +
            "for='piquet-" + i + "'>Piquet n°" + (i + 1) + " : </label>" +
            "<input class='text-center form-control w-25 d-inline piquetX' " +
            "required type='text' value='0' name='piquet-" + i + "'></input>" +
            "<input class='text-center form-control w-25 d-inline piquetY' " +
            "required type='text' value='0' name='piquet-" + i + "'></input>" +
            "</div>")
}

/**
 * Supprime des cases input si nécessaire
 * @param {number} nbPiquets - Nombre de piquets demandé
 * @param {number} nbPiquetsActuels - Nombre de piquets avant modification
 */
function suppressionInput(nbPiquets, nbPiquetsActuels) {
    if (nbPiquetsActuels > nbPiquets) // Si trop de case, suppression des dernières
        for (let i = nbPiquetsActuels; i > nbPiquets; i--)
            $(".piquet")[i - 1].remove()
}

/**
 * Redéfini les événements liés aux cases input de saisie de coordonnées
 */
function actualisationEventsInputs() {
    $(".piquet input").on("input",function() {
        let coordonnee = $(this).val()
        // Si pas un nombre (sauf - pour négatif et vide pour remplacer)
        if (!$.isNumeric(coordonnee) && coordonnee !== "-" && coordonnee !== "")
            $(this).val(0) // Valeur forcé à 0
    })
    $(".piquet input").blur(function() { // A la perte de focus d'une case de coordonnee
        let coordonnee = $(this).val()
        // Si pas un nombre
        if (!$.isNumeric(coordonnee))
            $(this).val(0) // Valeur forcé à 0
    })
}

/**
 * Lors de la validation, stockage des coordonnes et demarrage du traitement
 */
$("#validation").click(() => {
    let coordonnees = []
    let piquetsX = $(".piquetX") // Toutes les cases de coordonneés X
    let piquetsY = $(".piquetY") // Toutes les cases de coordonneés Y

    // Ajout des coordonées dans un tableau
    for (let i = 0; i < piquetsX.length; i++)
        coordonnees.push([parseFloat(piquetsX[i].value), parseFloat(piquetsY[i].value)])
    traitementDonnees(coordonnees)
})

/**
 * Différent calculs de traitement des données et demande d'affichage
 */
function traitementDonnees(coordonnees) { // Traitement des données et calcul
    let aire = calculAire(coordonnees)
    let centreGravite = calculCentreGravite(coordonnees, aire)
    let vacheDansEnclos = appartenancePointPolygone(coordonnees, centreGravite)

    affichageResultats(aire, centreGravite, vacheDansEnclos)
    initCanvas(coordonnees,centreGravite)
}

/**
 * Affichage des résultats finaux
 */
function affichageResultats(aire, centreGravite, vacheDansEnclos) {
    let centreGraviteAffichage = []

    centreGraviteAffichage[0] = centreGravite[0].toFixed(3)//Limitation du nombre de chiffres après la virgule
    centreGraviteAffichage[1] = centreGravite[1].toFixed(3)
    $("#aire").text(Math.abs(aire.toFixed(3)))// Ecriture des résultats
    $("#centre").text(centreGraviteAffichage)
    if(!isNaN(centreGravite[0]) && !isNaN(centreGravite[1])
        && (isFinite(centreGravite[0] && isFinite(centreGravite[1])))){
        if(vacheDansEnclos)
            $("#vacheDansEnclos").text("La vache est bien dans le pré !")
        else
            $("#vacheDansEnclos").text("Attention, la vache est hors du pré !")
        $("#resultat").show() // Affichage des résultats
    }else{
        $("#resultat").hide() // Masquage des résultats
        alert("Veuillez saisir les coordonnées dans l'ordre")
    }
}

/**
 * Calcul de l'aire du polygone (pré)
 * @param {Array.<number>} coordonnees - Coordonnées de tous les piquets
 * @return {number} Aire du polygone (pré)
 */
function calculAire(coordonnees) {
    let nombrePiquets = coordonnees.length
    let aire = 0

    for (let i = 0; i < nombrePiquets; i++)
        aire += coordonnees[i][0] * coordonnees[(i+1) % nombrePiquets][1]
            - coordonnees[(i+1) % nombrePiquets][0] * coordonnees[i][1]
    return aire / 2
}

/**
 * Calcul du centre de gravité du polygone (pré)
 * @param {Array.<number>} coordonnees - Coordonnées de tous les piquets
 * @param {number} aire - Aire du polygone (pré)
 * @return {Array.<number>} Coordonnées du centre de gravité du polygone (pré)
 */
function calculCentreGravite(coordonnees, aire) {
    let nombrePiquets = coordonnees.length
    let centreGravite = [0,0]

    for (let i = 0; i < nombrePiquets; i++) { // Mise à jour du point avec chaque piquet
        centreGravite[0] += (coordonnees[i][0] + coordonnees[(i+1) % nombrePiquets][0])
            * (coordonnees[i][0] * coordonnees[(i+1) % nombrePiquets][1]
                - coordonnees[(i+1) % nombrePiquets][0] * coordonnees[i][1])
        centreGravite[1] += (coordonnees[i][1] + coordonnees[(i+1) % nombrePiquets][1])
            * (coordonnees[i][0] * coordonnees[(i+1) % nombrePiquets][1]
                - coordonnees[(i+1) % nombrePiquets][0] * coordonnees[i][1])
    }
    centreGravite[0] /= 6 * aire
    centreGravite[1] /= 6 * aire
    return centreGravite
}

/**
 * Vérification de l'appartenance d'un point à un polygone (pré)
 * @param {Array.<number>} polygone - Coordonnées de tous les points (piquets)
 * @param {Array.<number>} point - Coordonnées du point à vérifier (ici centre de gravité)
 * @return {boolean} Appartenance du point au polygone (pré)
 */
function appartenancePointPolygone(polygone, point){
    let nombrePiquets = polygone.length
    let somme = 0
    let vecteurGS1 = []
    let vecteurGS2 = []

    for (let i = 0; i < nombrePiquets; i++) {
        for (let j=0; j < polygone[i].length; j++) { // Calcul des vecteurs GS_i et GS_i+1
            vecteurGS1[j] = polygone[i][j] - point[j]
            vecteurGS2[j] = polygone[(i+1) % nombrePiquets][j] - point[j]
        }
        let prodScalaire = vecteurGS1[0] * vecteurGS2[0] + vecteurGS1[1] * vecteurGS2[1]
        somme = calculDeterminant(vecteurGS1, vecteurGS2, somme, prodScalaire)
    }
    return somme !== 0
}

/**
 * Calcul du déterminant entre 2 sommets consécutifs
 * @param {Array.<number>} vecteurGS1 - Vecteur du point G au point S du polygone, correspond à la droite entre le centre de gravité et le 1er sommet
 * @param {Array.<number>} vecteurGS2 - Vecteur du point G au point S+1 du polygone, correspond à la droite entre le centre de gravité et le 2e sommet
 * @param {number} somme - Somme des angles du polygone
 * @param {number} prodScalaire - produit scalaire entre le vecteur GSi et le vecteur GSi+1 soit 2 sommets consécutifs du polygone
 * @returns {somme} Somme des angles du polygone
 */
function calculDeterminant(vecteurGS1, vecteurGS2,somme, prodScalaire){
    // Calcul des normes des vecteurs
    let vecteurGS1Norme = Math.sqrt(Math.pow(vecteurGS1[0],2)+ Math.pow(vecteurGS1[1],2))
    let vecteurGS2Norme = Math.sqrt(Math.pow(vecteurGS2[0],2)+ Math.pow(vecteurGS2[1],2))
    let theta = Math.acos(prodScalaire / (vecteurGS1Norme * vecteurGS2Norme))//Calcul de l'angle theta entre 2 sommets consécutifs

    if((vecteurGS1[0] * vecteurGS2[1] - vecteurGS1[1] * vecteurGS2[0]) < 0)//Si le déterminant est négatif
        somme -= theta
    else
        somme += theta
    return somme
}

//Variable globale d'échelle dans le canvas qui est accessible par chaque méthode
var scale

/**
 * Initialisation du canvas où le pré et la vache seront dessinées
 * @param {Array.<number>} coordonnees - Coordonnées de tous les points (piquets)
 * @param {Array.<number>} centreGravite - Coordonnées du centre de gravité du polygone (pré)
 */
function initCanvas(coordonnees,centreGravite){
    scale = 1.0
    var canvas = document.getElementById("myCanvas")
    var translatePos = { // Centrage initial du pré sur le centre de gravité
        x: (canvas.width / 2) - (centreGravite[0] * 10) - 7,
        y: canvas.height / 2 - (centreGravite[1] * 10) + 1}

    canvas.addEventListener("wheel", event => { // Lors d'un scroll
        // Multiplication de la taille si scroll avant ("zoom"). Sinon, division
        (Math.sign(event.deltaY) < 0) ? scale /= 0.9 : scale *= 0.9
        draw(canvas, translatePos ,coordonnees, centreGravite)
        event.preventDefault() // Retrait du comportement par défaut de l'évenement (navigateur...)
    }, {passive: false})
    initEvent(canvas, translatePos, coordonnees, centreGravite)
    draw(canvas, translatePos,coordonnees, centreGravite) // Actualisation
}

/**
 * Initialisation des événements liés au zoom/déplacement du pré
 * @param {canvas} canvas - Canvas où la vache et le pré seront dessinées
 * @param {Array.<number>} translatePos - Position en X et Y de la caméra dans le canvas
 * @param {Array.<number>} coordonnees - Coordonnées de tous les points (piquets)
 * @param {Array.<number>} centreGravite - Coordonnées du centre de gravité du polygone (pré)
 */
function initEvent(canvas, translatePos, coordonnees, centreGravite){
    var startDragOffset = {}

    mouseDownEvent(canvas, translatePos, startDragOffset)
    mouseUpEvent(canvas)
    mouseOverEvent(canvas)
    mouseOutEvent(canvas)
    mouseMoveEvent(canvas, coordonnees, centreGravite, translatePos, startDragOffset)
}

/**
 * Gestion du clic de la souris sur le canvas
 * @param {canvas} canvas - Canvas où la vache et le pré seront dessinées
 * @param {Array.<number>} translatePos - Position de la caméra en X et Y sur le canvas
 * @param {Array.<number>} startDragOffset - Décalage en X et Y de la caméra pour gérer le déplacement à l'intérieur du canvas
 */
function mouseDownEvent(canvas, translatePos, startDragOffset){
    canvas.addEventListener("mousedown", (evt) => {
        mouseDown = true;
        startDragOffset.x = evt.clientX - translatePos.x
        startDragOffset.y = evt.clientY - translatePos.y
    })
}

/**
 * Gestion du relâchement du clic au dessus du canvas
 * @param {canvas} canvas - Canvas où la vache et le pré seront dessinées
 */
function mouseUpEvent(canvas){
    canvas.addEventListener("mouseup", () => {
        mouseDown = false
    });
}

/**
 * Gestion du survol du canvas
 * @param {canvas} canvas - Canvas où la vache et le pré seront dessinées
 */
function mouseOverEvent(canvas){
    canvas.addEventListener("mouseover", () => {
        mouseDown = false
    });
}

/**
 * Lorsque l'utilisateur quitte le survol du canvas
 * @param {canvas} canvas - Canvas où la vache et le pré seront dessinées
 */
function mouseOutEvent(canvas){
    canvas.addEventListener("mouseout", () => {
        mouseDown = false
    });
}

/**
 * Gestion du déplacement de la souris avec le clic enfoncé
 * @param {canvas} canvas - Canvas où la vache et le pré seront dessinées
 * @param {Array.<number>} coordonnees - Coordonnées de tous les points (piquets)
 * @param {Array.<number>} centreGravite - Coordonnées du centre de gravité du polygone (pré)
 * @param {Array.<number>} translatePos - Position en X et Y de la caméra dans le canvas
 * @param {Array.<number>} startDragOffset - Décalage en X et Y de la caméra pour gérer le déplacement à l'intérieur du canvas
 */
function mouseMoveEvent(canvas, coordonnees, centreGravite, translatePos, startDragOffset){
    canvas.addEventListener("mousemove", (evt) => {
        if (mouseDown) { // Si la souris se déplace et l'utilisateur clique, alors translation
            translatePos.x = evt.clientX - startDragOffset.x
            translatePos.y = evt.clientY - startDragOffset.y
            draw(canvas, translatePos,coordonnees, centreGravite)
        }
    });
}

/**
 * Dessin du pré et de la vache en fonction des points
 * @param {canvas} canvas - Canvas où la vache et le pré seront dessinées
 * @param {Array.<number>} translatePos - Déplacement du contenu dans le canvas
 * @param {Array.<number>} coordonnees - Coordonnées de tous les points (piquets)
 * @param {Array.<number>} centreGravite - Coordonnées du centre de gravité du polygone (pré)
 */
function draw(canvas, translatePos,coordonnees, centreGravite){
    var context = canvas.getContext("2d")
    
    context.clearRect(0, 0, canvas.width, canvas.height)// clear canvas
    context.save()
    context.translate(translatePos.x, translatePos.y) // Position
    context.scale(scale, scale) // Zoom
    context.beginPath()
    for(var i = 0; i < coordonnees.length; i++) // Création de la cloture
        context.lineTo(coordonnees[i][0] * 10, coordonnees[i][1] * 10)
    context.closePath()
    context.fillStyle = "#86BF39" // Couleurs pour une meilleure lisibilité
    context.strokeStyle = "#815744"
    context.stroke()
    context.fill()
    context.fillText("🐄", (centreGravite[0] * 10) - 7, (centreGravite[1] * 10) + 1) // Vache sur le centre de gravité
    context.restore()
}